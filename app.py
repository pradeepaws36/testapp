from flask import Flask
import sys 
app = Flask(__name__)
 
 
@app.route('/')
def hello_Team():
    return 'Team, Hello there! this is %s env and I have build 3' % sys.argv[1]
 
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
